from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
        list = form.save()
        return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=list)

    context = {
        "todo_detail": list,
        "form": form,
    }
    return render(request, "todos/modify.html", context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/newtask.html", context)


def todo_item_update(request, id):
    todo = TodoItem.objects.get(id=id)
    # list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm(instance=todo)

    context = {
        "todo_detail": todo,
        "form": form,
    }
    return render(request, "todos/modify.html", context)
# booooooo
